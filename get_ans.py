
import sys
import re

fname = sys.argv[1]

with open (fname, 'r') as f:
    data = f.read ()
    patt = re.compile (r'right_answers\s=\s"(.*?)"')
    
    print ("Found: %d" % len (patt.findall (data)))
    
    with open (fname+'_answers.txt', 'w') as ff:
        for k in patt.findall (data):
            ff.write (k+'\n')
