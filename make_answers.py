
import sys

fname = sys.argv[1]

with open (fname, 'r') as tf:
    data = tf.read ()

with open ('answers.txt', 'a') as f:
    f.write ("===========%s===========\n" % fname)
    f.write (data)
    f.write ('\n')
