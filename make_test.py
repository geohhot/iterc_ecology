import random
import re
import sys
import hashlib

fname = sys.argv[1]
data = None

with open (fname, 'r') as f:
    data = f.read ()

items = re.compile (r'<item(.*?)(?:</item>)', re.S)
question_text = re.compile (r'<question_text><!\[CDATA\[(.*?)\]')
anses = re.compile (r'<texts text="(.*?)" tt = "(.*?)"')

def conv_str2bool (t):
    if t == "False":
        return False
    return True

def shuffle (p):
    res = list (p)
    random.shuffle (res)
    return res

class question (object):
    def checkformorethanoneanswers (self):
        tf = 0
        for p in self.answers:
            if p[1] == '1':
                tf = tf + 1
        if tf > 1:
            print ("Weird question found")
            
    def __init__ (self, pars):
        self.text = question_text.findall (pars)[0]
        self.answers = list (map (lambda f: (f[0], conv_str2bool(f[1])), anses.findall (pars)))
        self.checkformorethanoneanswers ()
        m = hashlib.md5()
        m.update (self.text.encode ('utf-8'))
        self.checksum = m.hexdigest ()
    def view (self):
        print ("> %s" % self.text)
        for a in self.answers:
            print ("\t [ ] %s" % a[0])
    def to_html (self):
        st = '''<div><h2>%s</h2>''' % self.text
        for a in shuffle (list (self.answers)):
            st += ''' <input type="radio" name='%s' true='%d' value="%s" onclick="check_ans(this)">%s <br>''' % (self.checksum,a[1], a[0], a[0])
        st += '</div>'
        return st
        
basehtml = '''
<head>
  <meta charset="UTF-8">
<script type='text/javascript'>
check_ans = function (e) {
if (e.attributes['true'].value === '1') {
// #e3f5dc
e.parentElement.style = "background-color: #e3f5dc";
} else {
// #ff6475
e.parentElement.style = "background-color: #ff6475";
}
}
</script>
</head> 
'''

ending = '''

'''
        
fd = items.findall (data)
print ("Questions: %d" % len (fd))

res_html = basehtml

for q in shuffle (map (lambda p: question (p), fd)):
    res_html += q.to_html ()

res_html += basehtml

with open (fname + '.html', 'w') as f:
    f.write(res_html)
    
